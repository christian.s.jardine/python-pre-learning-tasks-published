def vowel_swapper(string):
    # ==============
    # Your code here
    swapTuple = {'a' : '4','e' : '3','i' : '!','o' : 'ooo','O': '000', 'u' : '|_|',
                 'A' : '4','E' : '3','I' : '!','U' : '|_|'
                }
    current_string = string
    for key, value in swapTuple.items():
        counterValue = string.count(key)
        if key != 'O' and counterValue >0:
            sliceIndex = current_string.lower().find(key)
            if sliceIndex >=0:
                newstring = current_string.lower()[sliceIndex+1:]
                sec_string = newstring.replace(key, value,1)
                current_string = current_string[0:sliceIndex+1] + sec_string
        elif key == 'O' and counterValue>0:
            sliceIndex = current_string.lower().find('o')
            if sliceIndex >=0:
                newstring = string[sliceIndex+1:]
                sec_string = newstring.replace(key, value,1)
                current_string = current_string[0:sliceIndex+1] + sec_string

    return current_string
    # ==============

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a4a e3e i!i o000o u|_|u" to the console
print(vowel_swapper("Hello World").title()) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available").title()) # Should print "Ev3rything's Av4!lable" to the console

#stringlong = "Everything's Available"
#print (stringlong[0:4] + stringlong[6:10])

