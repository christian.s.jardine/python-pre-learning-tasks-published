def factors(number):
    # ==============
    # Your code here
    primary_list = []
    for i in range(1,number):
        if number % i == 0 and i != 1:
            primary_list.append(i)
        else:
            pass
    if len(primary_list) > 0:
        return primary_list
    elif number == 0 or number == 1:
        return f"{number} is neither prime nor composite number"
    else:
        return f"{number} is a prime number"
    # ==============

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “13 is a prime number”
